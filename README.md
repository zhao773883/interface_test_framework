# interface_test_framework

#### 介绍
开源的接口自动化测试框架

#### 软件架构
使用了 requests库 unittest测试框架以及unittestreport扩展库。

####模块介绍
api模块主要是处理 api地址和api涉及相关处理,如headers，json的处理。

config模块主要是存放配置文件。

data模块主要是存放一些测试数据，如yaml文件，接送文件。
  
public模块主要是存放requests库的基类和一些公共类。

testcase模块主要是存放测试用例。

tools模块主要是存放的一些工具类，如读取配置文件，获取token。

report文件主要是存放一些日志文件和测试报告。

main函数，主函数入口，所有模块集成运行。

这个框架基本上能满足简单的接口测试业务，如果需要使用测试数据请在testcase中读取数据，我提供3种数据方式，一种是通过sql当数据容器，json文件和yaml文件也可以当数据容器。
希望大家看我的思路对你有些帮助。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
