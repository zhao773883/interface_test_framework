# -*- coding: utf-8 -*-
"""
@Time ： 2021/3/22 20:35
@Auth ： 买菠萝凤梨
@File ：get_token.py
@IDE ：PyCharm
@Motto：work steadily

"""
from public.base_requests import Requests
from public.log import Log
log = Log()


class Before:
    def __init__(self, url, username, password):
        try:
            '''前置操作，通过登录获取登录的token'''
            payload = {'username': username,
                       'password': password,
                       }
            header = {
                "Content-Type": "application/json;charset=UTF-8"
            }
            self.response = Requests().post(url, json=payload, headers=header)
            log.info('初始化登录成功')
        except Exception as e:
            log.error('初始化登录失败：{}'.format(e))

    def get_token(self):
        data = self.response.json()['data']['access_token']
        return data


if __name__ == '__main__':
    res = Before().get_token()
    print(res)



