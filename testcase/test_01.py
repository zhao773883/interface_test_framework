# -*- coding: utf-8 -*-
"""
@Time ： 2021/3/25 14:55
@Auth ： 买菠萝凤梨
@File ：test_01.py
@IDE ：PyCharm
@Motto：work steadily

"""
import unittest
from unittestreport import rerun
from public.log import Log
from api.api_operation.api_operation import Opeartion
from tools.get_token import Before


class Test_api(unittest.TestCase):

    def setUp(self):
        self.token = Before().get_token()

    def tearDown(self):
        pass

    @rerun(count=4, interval=2) # 如果用例执行失败，会尝试4次重新执行，每次间隔时间为2s
    def test_journals(self):
        jour = Opeartion().journals('测试', self.token)
        if jour['status'] == '200':
            return True
        self.assertTrue(jour)


if __name__ == '__main__':
    unittest.main()
