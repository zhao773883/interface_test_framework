# -*- coding: utf-8 -*-
"""
@Time ： 2021/3/22 16:58
@Auth ： 买菠萝凤梨
@File ：api_operation.py
@IDE ：PyCharm
@Motto：work steadily

"""
'''这个模块主要是处理对具体接口的操作'''
from api.api_url.api_url import API_url
from public.log import Log

log = Log()


class Opeartion:

    def __init__(self):
        self.api = API_url()

    def login(self, username, password):
        try:
            payload = {'username': username,
                       'password': password,
                       }

            header = {
                "Content-Type": "application/json;charset=UTF-8"
            }
            data = self.api.login(json=payload, headers=header).json()
            return data
        except Exception as e:
            log.error('登录接口异常：{}'.format(e))

    def journals(self, text, token):
        try:
            payload = {"sourceContent": text}
            header = {"Content-Type": "application/json;charset=UTF-8",
                      "Admin-Authorization": token}
            res = self.api.journals(json=payload, headers=header).json()
            log.info('速记接口请求成功')
            return res
        except Exception as e:
            log.error('接口出现异常：{}'.format(e))


if __name__ == '__main__':
    # data = Opeartion().login('菠萝', 'xiecong000')
    # print(data)
    from tools.get_token import Before
    token = Before().get_token()
    data = Opeartion().journals('测试',token)
    print(data)
