# -*- coding: utf-8 -*-
"""
@Time ： 2021/3/22 16:41
@Auth ： 买菠萝凤梨
@File ：api_url.py
@IDE ：PyCharm
@Motto：work steadily

"""
from public.log import Log
from tools.read_config import Read_conf
from public.base_requests import Requests

root_url = Read_conf().load_ini('test.ini')['ROOT_URL']['root_url']



log = Log()


class API_url(Requests):

    def __init__(self):
        super(API_url,self).__init__()
        self.url = root_url


    def login(self,**kwargs):
        '''登录地址的url'''
        return self.post(self.url+'/api/admin/login',**kwargs)

    def journals(self, **kwargs):
        '''速记接口'''
        return self.post(self.url+'/api/admin/journals',**kwargs)
